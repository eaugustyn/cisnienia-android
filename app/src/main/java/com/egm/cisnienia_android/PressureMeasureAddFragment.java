package com.egm.cisnienia_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.egm.cisnienia_android.validators.DiastolicPredicate;
import com.egm.cisnienia_android.validators.PulsePredicate;
import com.egm.cisnienia_android.validators.SystolicPredicate;
import com.egm.cisnienia_android.validators.Validators;
import com.egm.medical.domain.model.blood_pressure.BloodPressure;
import com.egm.medical.domain.model.blood_pressure.Location;
import com.egm.medical.domain.model.blood_pressure.Position;
import com.egm.medical.domain.model.blood_pressure.PressureMeasure;
import com.egm.medical.infrastructure.persistence.repository.SQLLittlePressureMeasureRepository;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.egm.cisnienia_android.util.UIUtil.getFromEditTextAsDateTime;
import static com.egm.cisnienia_android.util.UIUtil.getFromEditTextAsInt;
import static com.egm.cisnienia_android.util.UIUtil.getFromEditTextAsString;
import static org.joda.time.format.DateTimeFormat.forPattern;

public class PressureMeasureAddFragment extends Fragment {

    private static final String TAG = "PressureMeasureAdd";
    private static final String DIALOG_MEASURE_DATE = "DialogMeasureDate";
    private static final String DIALOG_MEASURE_TIME = "DialogMeasureTime";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_TIME = 1;

    private Toolbar mToolbar;

    private EditText mMeasureDateEditText;
    private EditText mMeasureTimeEditText;

    private TextInputLayout mSystolicLayout;
    private EditText mSystolicEditText;

    private TextInputLayout mDiastolicLayout;
    private EditText mDiastolicEditText;

    private TextInputLayout mPulseLayout;
    private EditText mPulseEditText;

    private EditText mDescriptionEditText;

    public static PressureMeasureAddFragment newInstance() {
        return new PressureMeasureAddFragment();
    }

    public PressureMeasureAddFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(Boolean.TRUE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pressure_measure_add, container, false);

        initToolbar(view);

        mMeasureDateEditText = (EditText) view.findViewById(R.id.edit_text_measure_date);
        DateTimeFormatter dtf1 = forPattern("yyyy-MM-dd");
        mMeasureDateEditText.setText(dtf1.print(DateTime.now()));
        mMeasureDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(new DatePickerFragment(), REQUEST_DATE, DIALOG_MEASURE_DATE);
            }
        });

        mMeasureTimeEditText = (EditText) view.findViewById(R.id.edit_text_measure_time);
        DateTimeFormatter dtf2 = forPattern("HH:mm");
        mMeasureTimeEditText.setText(dtf2.print(DateTime.now()));
        mMeasureTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(new TimePickerFragment(), REQUEST_TIME, DIALOG_MEASURE_TIME);
            }
        });

        mSystolicLayout = (TextInputLayout) view.findViewById(R.id.input_layout_systolic);
        mSystolicEditText = (EditText) view.findViewById(R.id.edit_text_systolic);

        mDiastolicLayout = (TextInputLayout) view.findViewById(R.id.input_layout_diastolic);
        mDiastolicEditText = (EditText) view.findViewById(R.id.edit_text_diastolic);

        mPulseLayout = (TextInputLayout) view.findViewById(R.id.input_layout_pulse);
        mPulseEditText = (EditText) view.findViewById(R.id.edit_text_pulse);

        mDescriptionEditText = (EditText) view.findViewById(R.id.edit_text_description);

        return view;
    }

    private void initToolbar(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void openDialog(DialogFragment dialog, int requestCode, String dialogCaption) {
        FragmentManager manager = getFragmentManager();
        dialog.setTargetFragment(PressureMeasureAddFragment.this, requestCode);
        dialog.show(manager, dialogCaption);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            mMeasureDateEditText.setText(df.format(date));
        } else if (requestCode == REQUEST_TIME) {
            Date time = (Date) data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
            DateFormat df = new SimpleDateFormat("HH:mm");
            mMeasureTimeEditText.setText(df.format(time));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_pressure_measure_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_save_measure:
                saveMeasure();
                return Boolean.TRUE;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveMeasure() {
        if (!isFormValid())
            return;


        int systolic = getFromEditTextAsInt(mSystolicEditText);
        int diastolic = getFromEditTextAsInt(mDiastolicEditText);
        int pulse = getFromEditTextAsInt(mPulseEditText);

        String description = getFromEditTextAsString(mDescriptionEditText);
        BloodPressure bp = BloodPressure.of(systolic, diastolic, pulse);

        DateTime measureDate = getFromEditTextAsDateTime(mMeasureDateEditText, forPattern("yyyy-MM-dd"));
        DateTime measureTime = getFromEditTextAsDateTime(mMeasureTimeEditText, forPattern("HH:mm"));

        LocalDate datePart = new LocalDate(measureDate);
        LocalTime timePart = new LocalTime(measureTime);
        LocalDateTime measureDateTime = datePart.toLocalDateTime(timePart);

        PressureMeasure pm = new PressureMeasure(measureDateTime.toDateTime(), bp, description, Location.LEFT_ARM, Position.HORIZONTAL);

        Log.i(TAG, pm.toString());

        new SQLLittlePressureMeasureRepository().save(pm);

        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    public boolean isFormValid() {

        boolean sysVal = Validators.systolic().apply(mSystolicEditText.getEditableText().toString());
        boolean diasVal = Validators.diastolic().apply(mDiastolicEditText.getEditableText().toString());
        boolean pulseVal = Validators.pulse().apply(mPulseEditText.getText().toString().trim());

        if (!sysVal)
            mSystolicLayout.setError(getText(SystolicPredicate.SYSTOLIC_VALIDATE_ERR_MSG));
        else
            mSystolicLayout.setErrorEnabled(false);

        if (!diasVal)
            mDiastolicLayout.setError(getText(DiastolicPredicate.DIASTOLIC_VALIDATE_ERR_MSG));
        else
            mDiastolicLayout.setErrorEnabled(false);

        if (!pulseVal)
            mPulseLayout.setError(getText(PulsePredicate.PULSE_VALIDATE_ERR_MSG));
        else
            mPulseLayout.setErrorEnabled(false);


        return sysVal && diasVal && pulseVal;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
