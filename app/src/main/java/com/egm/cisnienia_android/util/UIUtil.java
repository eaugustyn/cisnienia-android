package com.egm.cisnienia_android.util;

import android.widget.EditText;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public class UIUtil {

    public static int getFromEditTextAsInt(EditText editText) {
        return Integer.parseInt(editText.getText().toString());
    }

    public static String getFromEditTextAsString(EditText editText) {
        return editText.getText().toString();
    }

    public static DateTime getFromEditTextAsDateTime(EditText editText, DateTimeFormatter formatter) {
        return formatter.parseDateTime(editText.getText().toString());
    }

}
