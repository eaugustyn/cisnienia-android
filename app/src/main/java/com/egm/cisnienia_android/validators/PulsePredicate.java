package com.egm.cisnienia_android.validators;

import com.android.internal.util.Predicate;
import com.egm.cisnienia_android.R;
import com.google.common.base.Strings;

public class PulsePredicate implements Predicate<String> {

    public static final int PULSE_VALIDATE_ERR_MSG = R.string.pulse_val_err;

    private static final int MAX_VAL = 250;
    private static final int MIN_VAL = 30;

    @Override
    public boolean apply(String s) {
        if (Strings.isNullOrEmpty(s))
            return false;

        int systolic = Integer.valueOf(s);

        return !(systolic > MAX_VAL || systolic < MIN_VAL);
    }

}
