package com.egm.cisnienia_android.validators;

import com.android.internal.util.Predicate;

public class Validators {

    private final static Predicate<String> systolic = new SystolicPredicate();
    private final static Predicate<String> diastolic = new DiastolicPredicate();
    private final static Predicate<String> pulse = new PulsePredicate();


    public static Predicate<String> systolic() {
        return systolic;
    }

    public static Predicate<String> diastolic() {
        return diastolic;
    }


    public static Predicate<String> pulse() {
        return pulse;
    }
}
