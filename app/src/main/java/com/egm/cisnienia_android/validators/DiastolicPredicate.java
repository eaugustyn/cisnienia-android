package com.egm.cisnienia_android.validators;

import com.android.internal.util.Predicate;
import com.egm.cisnienia_android.R;
import com.google.common.base.Strings;

public class DiastolicPredicate implements Predicate<String>  {
    public static final int DIASTOLIC_VALIDATE_ERR_MSG = R.string.diastolic_val_err;

    private static final int MAX_VAL = 150;
    private static final int MIN_VAL = 30;

    @Override
    public boolean apply(String s) {
        if (Strings.isNullOrEmpty(s))
            return false;

        int systolic = Integer.valueOf(s);

        return !(systolic > MAX_VAL || systolic < MIN_VAL);
    }

}
