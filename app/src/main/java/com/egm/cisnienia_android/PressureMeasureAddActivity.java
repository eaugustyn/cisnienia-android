package com.egm.cisnienia_android;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PressureMeasureAddActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return PressureMeasureAddFragment.newInstance();
    }

}
