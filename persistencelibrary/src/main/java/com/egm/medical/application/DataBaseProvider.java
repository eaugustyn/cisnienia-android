package com.egm.medical.application;


import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.egm.medical.infrastructure.persistence.database.DataBaseHelper;

public class DataBaseProvider extends Application {

    private static SQLiteDatabase sqlDatabase;

    public static SQLiteDatabase getSqlDatabase() {
        return sqlDatabase;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sqlDatabase = new DataBaseHelper(getApplicationContext()).getWritableDatabase();
    }
}
