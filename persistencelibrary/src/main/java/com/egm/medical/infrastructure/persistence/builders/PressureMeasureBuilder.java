package com.egm.medical.infrastructure.persistence.builders;

import android.database.Cursor;

import com.egm.medical.domain.model.blood_pressure.BloodPressure;
import com.egm.medical.domain.model.blood_pressure.Location;
import com.egm.medical.domain.model.blood_pressure.Position;
import com.egm.medical.domain.model.blood_pressure.PressureMeasure;

import static com.egm.medical.infrastructure.persistence.entry.PressureMeasureEntry.*;

import com.egm.medical.infrastructure.persistence.repository.CursorBuilder;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class PressureMeasureBuilder implements CursorBuilder<PressureMeasure> {

    private final static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public PressureMeasure build(Cursor c) {
        BloodPressure bp = BloodPressure.of(getInt(c, COLUMN_SYSTOLIC),
                getInt(c, COLUMN_DIASTOLIC),
                getInt(c, COLUMN_PULSE));
        DateTime measureDate = formatter.parseDateTime(getString(c, COLUMN_MEASURE_DATE));
        String description = getString(c, COLUMN_DESCRIPTION);
        Location location = Location.of(getString(c, COLUMN_LOCATION));
        Position position = Position.of(getString(c, COLUMN_POSITION));

        return new PressureMeasure(measureDate, bp, description, location, position);
    }

    private int getInt(Cursor c, String column) {
        return c.getInt(c.getColumnIndexOrThrow(column));
    }

    private String getString(Cursor c, String column) {
        return c.getString(c.getColumnIndexOrThrow(column));
    }

}
