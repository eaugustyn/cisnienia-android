package com.egm.medical.infrastructure.persistence.repository;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.egm.medical.application.DataBaseProvider;

import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.List;

public abstract class AbstractRepository {

    public SQLiteDatabase getDb() {
        return DataBaseProvider.getSqlDatabase();
    }

    protected <T> List<T> buildDomainModelList(Cursor cursor, CursorBuilder<T> builder) {
        List<T> list = Lists.newArrayList();

        while (cursor.moveToNext())
            list.add(builder.build(cursor));

        return list;
    }

    protected <T> T buildDomainModel(Cursor cursor, CursorBuilder<T> builder) {
        if (cursor.moveToFirst())
            return builder.build(cursor);

        return null;
    }

    protected void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

}
