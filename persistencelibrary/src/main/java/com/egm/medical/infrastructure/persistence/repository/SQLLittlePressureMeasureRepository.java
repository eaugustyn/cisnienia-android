package com.egm.medical.infrastructure.persistence.repository;

import android.content.ContentValues;
import android.database.Cursor;

import com.egm.medical.domain.model.blood_pressure.PressureMeasure;
import com.egm.medical.domain.model.blood_pressure.PressureMeasureRepository;
import com.egm.medical.infrastructure.persistence.builders.PressureMeasureBuilder;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import static com.egm.medical.infrastructure.persistence.entry.PressureMeasureEntry.*;

import java.util.List;

public class SQLLittlePressureMeasureRepository extends AbstractRepository implements PressureMeasureRepository {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public List<PressureMeasure> getAll() {
        Cursor cursor = null;
        try {
            cursor = getDb().query(TABLE_NAME,
                    prepareProjections(),
                    null,
                    null,
                    null,
                    null,
                    prepareOrderBy());
            return buildDomainModelList(cursor, new PressureMeasureBuilder());
        } finally {
            closeCursor(cursor);
        }
    }


    @Override
    public PressureMeasure getByMeasureDate(DateTime measureDate) {
        Cursor cursor = null;
        try {
            cursor = getDb().query(TABLE_NAME,
                    prepareProjections(),
                    SELECTION_MEASURE_DATE,
                    new String[]{DATE_TIME_FORMATTER.print(measureDate)},
                    null,
                    null,
                    prepareOrderBy());

            return buildDomainModel(cursor, new PressureMeasureBuilder());
        } finally {
            closeCursor(cursor);
        }
    }

    @Override
    public void save(PressureMeasure pressureMeasure) {
        getDb().insert(TABLE_NAME, null, getContentValue(pressureMeasure));
    }

    private ContentValues getContentValue(PressureMeasure pressureMeasure) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MEASURE_DATE, DATE_TIME_FORMATTER.print(pressureMeasure.getMeasureDate()));
        values.put(COLUMN_SYSTOLIC, pressureMeasure.getBloodPressure().systolic());
        values.put(COLUMN_DIASTOLIC, pressureMeasure.getBloodPressure().diastolic());
        values.put(COLUMN_PULSE, pressureMeasure.getBloodPressure().pulse());
        values.put(COLUMN_DESCRIPTION, pressureMeasure.getDescription());
        values.put(COLUMN_LOCATION, pressureMeasure.getLocation().getDbCode());
        values.put(COLUMN_POSITION, pressureMeasure.getPosition().getDbCode());
        return values;
    }

    @Override
    public void deleteByMeasureDate(DateTime measureDate) {
        getDb().delete(TABLE_NAME, SELECTION_MEASURE_DATE, new String[]{DATE_TIME_FORMATTER.print(measureDate)});
    }

}
